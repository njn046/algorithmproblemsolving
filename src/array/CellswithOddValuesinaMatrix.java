package array;

import java.util.*;

class CellswithOddValuesinaMatrix {
    public static void main (String[] args) {
    	
    	int n = 2;
    	int m = 3;
    	int[][] indices = {
    			{0, 1},
    			{1, 1}
    	};
    	
    	System.out.println(oddCells(n, m, indices));	
    }
    
    public static int oddCells(int n, int m, int[][] indices) {
    	int[][] init = new int[n][m];
    	for (int[] row : init)    		
    		Arrays.fill(row, 0);

    	for (int[] row : indices) {
			int row_index = row[0];
    		int col_index = row[1];
    		
    		int i = 0;
    		while (i < m) {
    			init[row_index][i] += 1;
    			i++;
    		}
    		
    		i = 0;
    		while (i < n) {
    			init[i][col_index] += 1;
    			i++;
    		}
    			
    	}
    	int counter = 0;
    	for (int[] row : init) {
    		for (int cell : row) {
    			if (cell % 2 != 0) {
    				counter++;
    			}
    		}
    	}
    	System.out.println(Arrays.toString(init));
        return counter;
    }
    
    
//    Solution from Discussion
    public int oddCellsDiscussion(int n, int m, int[][] indices) {
        boolean[] oddRows = new boolean[n], oddCols = new boolean[m];
        for (int[] idx : indices) {
            oddRows[idx[0]] ^= true; // if row idx[0] appears odd times, it will correspond to true.
            oddCols[idx[1]] ^= true; // if column idx[1] appears odd times, it will correspond to true.
        }
        int cnt = 0;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                cnt += oddRows[i] ^ oddCols[j] ? 1 : 0; // only cell (i, j) with odd times count of row + column would get odd values.
            }
        }
        return cnt;
    }
}