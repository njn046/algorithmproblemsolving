package array;

import java.util.*;

public class FindNUniqueIntegersSumuptoZero {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int n = 10;
		System.out.println(Arrays.toString(sumZero(n)));
	}
	
    public static int[] sumZero(int n) {
        int[] arr = new int[n];
        
        int i = 0;
        while (i < n) {
            arr[i] = i * 2 - n + 1;
            i++;
        }
        
        return arr;
    }

}
