package array;

import java.util.Arrays;

public class MoveZeroes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] input = { 1, 0, 0, 2, 5 };
		System.out.println(Arrays.toString(moveZeroes(input)));
	}
	
	public static int[] moveZeroes(int[] nums) {
        int position = 0;
        
        for (int num : nums) {
            if (num != 0) nums[position++] = num;
        }
        
        while (position < nums.length) {
            nums[position++] = 0;
        }
        
        return nums;
    }
}
